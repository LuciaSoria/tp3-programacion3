package interfaz;

import java.awt.Color;
import java.util.List;

import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import negocio.Coordenada;
import negocio.RadioCensal;

public class MapaPanel extends JPanel {

	private JMapViewer _mapa;
	private String[] _censistasManzanas;
	private List<Coordenada> _coordenadas;
	private MapMarker _marker;
	private RadioCensal _radioCensal;

	public MapaPanel() {
		setLayout(null);
		initialize();
	}

	public void geolocalizarCensistas() {
		_censistasManzanas = _radioCensal.getCensistasManzanas();
		_coordenadas = _radioCensal.getCoordenadas();
		ubicarCensistas();
		centrarUbicacion();
	}

	public void setRadioCensal(RadioCensal radioCensal) {
		this._radioCensal = radioCensal;
	}

	private void ubicarCensistas() {
		Coordenada coordenada;
		Coordinate coor;
		for (int i = 0; i < _coordenadas.size(); i++) {
			coordenada = _coordenadas.get(i);
			coor = new Coordinate(coordenada.get_x(), coordenada.get_y());
			String censista = _censistasManzanas[i];
			_marker = new MapMarkerDot(censista, coor);
			this._mapa.addMapMarker(_marker);
		}
	}

	protected void centrarUbicacion() {
		Coordinate coor = _radioCensal.calcularCentro();
		_mapa.setDisplayPosition(coor, 16);
	}

	private void initialize() {
		this.setBounds(0, 0, 300, 273);
		_mapa = new JMapViewer();
		_mapa.setBounds(0, 0, 300, 273);
		_mapa.setZoomControlsVisible(false);
		this.add(_mapa);
		_marker = new MapMarkerDot("", null);
		_marker.getStyle().setBackColor(Color.blue);
	}
}