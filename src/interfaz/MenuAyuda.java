package interfaz;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class MenuAyuda extends JPanel {

	private Image _imagen;
	private Image _imagenFondo;

	public MenuAyuda() {
		setLayout(null);
		initialize();
	}

	private void initialize() {
		JLabel textoTitulo = new JLabel("Formato del JSON");
		textoTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		textoTitulo.setBounds(319, 0, 164, 14);
		add(textoTitulo);
	}

	public void paint(Graphics graphics) {
		_imagenFondo = new ImageIcon(getClass().getResource("/imagenFondo.jpg")).getImage();
		graphics.drawImage(_imagenFondo, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
		_imagen = new ImageIcon(getClass().getResource("/imagenFormatoJSON.png")).getImage();
		graphics.drawImage(_imagen, 200, 30, 400, 380, this);
		setOpaque(false);
		super.paint(graphics);
	}
}
