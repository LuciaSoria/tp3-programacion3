package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import negocio.*;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.SwingConstants;

public class MenuArchivo extends JPanel {
	private RadioCensal _radioCensal;
	private JButton _botonAgregarArchivo;
	private JLabel _opcionTexto;
	private JLabel _textoArchivo;
	private JLabel _indicacionDelFormato;
	private JFileChooser _fileChooser;
	private Scanner _entrada;
	private Image _imagenArchivo;
	private Image _imagen;

	public MenuArchivo() {
		setLayout(null);
		initialize();
	}

	private void initialize() {
		_botonAgregarArchivo = new JButton("Seleccionar Archivo");
		_botonAgregarArchivo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		_botonAgregarArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pedirArchivo();
			}
		});
		_botonAgregarArchivo.setBounds(277, 242, 195, 34);
		add(_botonAgregarArchivo);

		_opcionTexto = new JLabel("o bien...");
		_opcionTexto.setHorizontalAlignment(SwingConstants.CENTER);
		_opcionTexto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		_opcionTexto.setBounds(332, 287, 83, 13);
		add(_opcionTexto);

		_textoArchivo = new JLabel("No selecciono un archivo");
		_textoArchivo.setHorizontalAlignment(SwingConstants.CENTER);
		_textoArchivo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		_textoArchivo.setBounds(296, 215, 176, 19);
		add(_textoArchivo);

		_indicacionDelFormato = new JLabel("Suba su archivo JSON");
		_indicacionDelFormato.setFont(new Font("Tahoma", Font.PLAIN, 15));
		_indicacionDelFormato.setBounds(305, 11, 273, 14);
		add(_indicacionDelFormato);
	}

	private void pedirArchivo() {
		_entrada = null;
		_fileChooser = new JFileChooser(".");
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos json (.json)", "json");
		_fileChooser.setFileFilter(filtro);
		int valor = _fileChooser.showOpenDialog(_fileChooser);
		if (valor == JFileChooser.APPROVE_OPTION) {
			String ruta = _fileChooser.getSelectedFile().getAbsolutePath();
			try {
				RadioCensal rc = RadioCensalDAO.leerJSON(ruta);
				_radioCensal = rc;
				_textoArchivo.setText(_fileChooser.getSelectedFile().getName());
			} catch (RuntimeException e) {
				JOptionPane.showMessageDialog(null, "El sistema no puede encontrar el archivo especificado");
			} finally {
				if (_entrada != null) {
					_entrada.close();
				}
			}
		}
	}

	public RadioCensal getRadioCensal() {
		return _radioCensal;
	}

	public void paint(Graphics graphics) {
		_imagen = new ImageIcon(getClass().getResource("/imagenFondo.jpg")).getImage();
		graphics.drawImage(_imagen, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
		_imagenArchivo = new ImageIcon(getClass().getResource("/imagenarchivo.png")).getImage();
		graphics.drawImage(_imagenArchivo, 305, 50, 150, 150, this);
		setOpaque(false);
		super.paint(graphics);
	}
}