package interfaz;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import negocio.RadioCensal;

public class MenuResultado extends JPanel {
	private RadioCensal _radioCensal;
	private JTable _table;
	protected MapaPanel _mapa;
	private Image _imagen;

	public MenuResultado() {
		setLayout(null);
		initialize();
	}

	private void initialize() {
		_radioCensal = new RadioCensal();
		resultadoJLabel();
		cargarTabla();

		_mapa = new MapaPanel();
		_mapa.setBounds(570, 55, 300, 273);
		this.add(_mapa);

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Censista");
		model.addColumn("Numero de manzanas");

		JButton botonResultado = new JButton("iniciar");
		botonResultado.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_radioCensal.asignarCensistas();
				Map<String, List<Integer>> manzanasPorCensistas = _radioCensal.manzanasPorCensista();
				for (String censista : manzanasPorCensistas.keySet()) {
					model.addRow(new String[] { censista, manzanasPorCensistas.get(censista).toString() });
				}
				botonResultado.setEnabled(false);
				_mapa.setRadioCensal(_radioCensal);
				_mapa.geolocalizarCensistas();
			}
		});
		botonResultado.setBounds(438, 10, 104, 31);
		add(botonResultado);

		_table.setModel(model);
	}

	private void resultadoJLabel() {
		JLabel textResultado = new JLabel("RESULTADO");
		textResultado.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textResultado.setBounds(295, 9, 124, 30);
		add(textResultado);
	}

	private void cargarTabla() {
		JScrollPane JS = new JScrollPane(_table);
		JS.setBounds(61, 55, 500, 273);
		this.add(JS);

		_table = new JTable();
		_table.setShowGrid(false);
		_table.setFont(new Font("Sylfaen", Font.LAYOUT_LEFT_TO_RIGHT, 16));
		_table.setGridColor(Color.LIGHT_GRAY);
		_table.setForeground(Color.BLACK);
		_table.setBorder(null);
		_table.setBounds(44, 81, 366, 208);
		_table.setRowHeight(30);
		_table.setOpaque(false);
		JS.setViewportView(_table);
	}

	public void setRadioCensal(RadioCensal radio) {
		this._radioCensal = radio;
	}

	public void paint(Graphics graphics) {
		_imagen = new ImageIcon(getClass().getResource("/imagenFondo.jpg")).getImage();
		graphics.drawImage(_imagen, 0, 0, getWidth(), getHeight(), this);
		setOpaque(false);
		super.paint(graphics);
	}
}