package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import negocio.RadioCensal;
import negocio.RadioCensalDAO;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MenuPrincipal extends JFrame {

	private MenuArchivo _menuArchivo;
	private MenuResultado _menuResultado;
	private MenuAyuda _menuAyuda;
	private RadioCensal _radioCensal;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuPrincipal() {
		this.setTitle("Asignacion de censistas");
		_menuArchivo = new MenuArchivo();
		_menuResultado = new MenuResultado();
		_menuAyuda = new MenuAyuda();
		_radioCensal = new RadioCensal();

		menuArchivo();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 450);
	}

	public void menuArchivo() {
		getContentPane().revalidate();
		setContentPane(_menuArchivo);

		JButton botonCalcular = new JButton("Calcular");
		botonCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (_menuArchivo.getRadioCensal() == null) {
					JOptionPane.showMessageDialog(null, "Debes seleccionar un archivo.");
				} else {
					_radioCensal = _menuArchivo.getRadioCensal();
					menuResultado();
				}
			}
		});
		botonCalcular.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonCalcular.setBounds(609, 355, 150, 30);
		_menuArchivo.add(botonCalcular);

		JButton botonArchivoEjemplo = new JButton("Use un archivo preestablecido");
		botonArchivoEjemplo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		botonArchivoEjemplo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String f = new File("resources/datosRC.json").getAbsolutePath();
				RadioCensal rc = RadioCensalDAO.leerJSON(f);
				_radioCensal = rc;
				menuResultado();
			}
		});
		botonArchivoEjemplo.setBounds(242, 309, 267, 35);
		getContentPane().add(botonArchivoEjemplo);

		JButton botonHelp = new JButton("FormatoJSON");
		botonHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuAyuda();
			}
		});
		botonHelp.setBounds(630, 11, 128, 23);
		_menuArchivo.add(botonHelp);
	}

	public void menuResultado() {
		_menuResultado.setRadioCensal(_radioCensal);
		getContentPane().revalidate();
		setContentPane(_menuResultado);
	}

	public void menuAyuda() {
		repaint();
		getContentPane().revalidate();
		setContentPane(_menuAyuda);

		JButton botonVolver = new JButton("Volver");
		botonVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuArchivo();
			}
		});
		botonVolver.setFont(new Font("Tahoma", Font.PLAIN, 11));
		botonVolver.setBounds(650, 355, 150, 30);
		_menuAyuda.add(botonVolver);
	}
}