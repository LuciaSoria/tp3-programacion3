package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import negocio.Coordenada;
import negocio.RadioCensal;

public class RadioCensalTest {

	private RadioCensal _radio;
	private String _nombre;

	@Before
	public void iniciar() {
		_radio = crearRadio();
		_nombre = "Juan";
	}

	@Test
	public void unirManzanas() {
		assertTrue(_radio.existeArista(0, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void unirManzanasInvalidas() {
		_radio.unirManzanasContiguas(1, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void unirManzanasNegativa() {
		_radio.unirManzanasContiguas(1, -2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void unirManzanasIguales() {
		_radio.unirManzanasContiguas(1, 1);
	}

	@Test
	public void unirManzanasRepetido() {
		_radio.unirManzanasContiguas(1, 2);
		assertTrue(_radio.existeArista(1, 2));
	}

	@Test
	public void agregarCensista() {
		_radio.agregarCensista(_nombre);
		assertTrue(_radio.getCensistas().contains(_nombre));
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarCensistaInvalido() {
		String nombre = "";
		_radio.agregarCensista(nombre);
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarCensistaRepetido() {
		_radio.agregarCensista(_nombre);
		String nombre2 = "Juan";
		_radio.agregarCensista(nombre2);
	}

	@Test
	public void agregarCoordenada() {
		Coordenada coor = new Coordenada(2, 5);
		_radio.agregarCoordenada(0, coor);
		assertTrue(_radio.getCoordenadas().contains(coor));
	}

	@Test(expected = IllegalArgumentException.class)
	public void agregarCoordenadaManzanaInvalida() {
		Coordenada coor = new Coordenada(1, 5);
		_radio.agregarCoordenada(10, coor);
	}

	public void agregarOtraCoordenada() {
		Coordenada coor1 = new Coordenada(1, 5);
		_radio.agregarCoordenada(0, coor1);
		Coordenada coor2 = new Coordenada(4, 8);
		_radio.agregarCoordenada(0, coor2);

		assertTrue(_radio.getCoordenadas().contains(coor2) && !_radio.getCoordenadas().contains(coor1));
	}

	@Test
	public void asignarCensista() {
		_radio.agregarCensista(_nombre);
		_radio.asignarCensista(_nombre, 0);

		assertEquals(_nombre, _radio.getCensistaDe(0));
	}

	@Test
	public void asignarCensistaNuevo() {
		_radio.asignarCensista(_nombre, 0);

		assertTrue(_radio.getCensistas().contains(_nombre));
	}

	@Test
	public void asignarOtroCensista() {
		_radio.asignarCensista(_nombre, 0);
		String nombre2 = "Pedro";
		_radio.asignarCensista(nombre2, 0);
		_radio.asignarCensista(nombre2, 0);

		assertEquals(nombre2, _radio.getCensistaDe(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void asignarCensistaNombreInvalido() {
		_radio.asignarCensista("", 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void asignarCensistaManzanaInvalida() {
		_radio.asignarCensista(_nombre, 10);
	}

	@Test
	public void vecinosSinCensistas() {
		_radio.asignarCensista(_nombre, 0);
		List<Integer> vecinosSinCensista = _radio.vecinosSinCensista(1);
		List<Integer> esperado = new ArrayList<>();
		esperado.add(2);
		esperado.add(5);

		assertEquals(esperado, vecinosSinCensista);
	}

	@Test(expected = IllegalArgumentException.class)
	public void vecinosSinCensistasInvalida() {
		_radio.vecinosSinCensista(10);
	}

	@Test
	public void vecinosSinCensistasVacio() {
		_radio.asignarCensista(_nombre, 0);
		_radio.asignarCensista(_nombre, 2);
		_radio.asignarCensista(_nombre, 5);
		List<Integer> vecinosSinCensista = _radio.vecinosSinCensista(1);
		List<Integer> esperado = new ArrayList<>();

		assertEquals(esperado, vecinosSinCensista);
	}

	@Test
	public void vecinosSinCensistasSinVecinos() {
		RadioCensal radio = new RadioCensal(3);
		radio.agregarArista(0, 1);
		List<Integer> vecinosSinCensista = radio.vecinosSinCensista(2);
		List<Integer> esperado = new ArrayList<>();

		assertEquals(esperado, vecinosSinCensista);
	}

	@Test
	public void manzanasPorCensistas() {
		_radio.asignarCensista(_nombre, 0);
		_radio.asignarCensista(_nombre, 2);
		_radio.asignarCensista(_nombre, 6);
		_radio.asignarCensista("Pedro", 1);
		_radio.asignarCensista("Pedro", 3);
		_radio.asignarCensista("Pedro", 4);
		_radio.agregarCensista("Carlos");
		_radio.agregarCensista("Mario");
		Map<String, List<Integer>> esperado = esperadoManzanasPorCensista();

		assertEquals(esperado, _radio.manzanasPorCensista());
	}

	@Test
	public void manzanasPorCensistasLleno() {
		_radio.asignarCensista(_nombre, 0);
		_radio.asignarCensista(_nombre, 2);
		_radio.asignarCensista(_nombre, 6);
		_radio.asignarCensista("Pedro", 1);
		_radio.asignarCensista("Pedro", 3);
		_radio.asignarCensista("Pedro", 4);
		_radio.asignarCensista("Carlos", 5);
		_radio.asignarCensista("Mario", 7);
		Map<String, List<Integer>> esperado = esperadoManzanasPorCensistaLleno();

		assertEquals(esperado, _radio.manzanasPorCensista());
	}

	@Test
	public void manzanasPorCensistasVacio() {
		_radio.agregarCensista("Carlos");
		_radio.agregarCensista("Mario");

		Map<String, List<Integer>> esperado = new HashMap<>();
		esperado.put("Carlos", new ArrayList<>());
		esperado.put("Mario", new ArrayList<>());

		assertEquals(esperado, _radio.manzanasPorCensista());
	}

	@Test
	public void manzanasDe() {
		_radio.asignarCensista(_nombre, 0);
		_radio.asignarCensista(_nombre, 5);
		List<Integer> esperado = new ArrayList<>();
		esperado.add(0);
		esperado.add(5);

		assertEquals(esperado, _radio.manzanasDe(_nombre));
	}

	@Test
	public void manzanasDeVacio() {
		_radio.agregarCensista(_nombre);
		List<Integer> esperado = new ArrayList<>();
		assertEquals(esperado, _radio.manzanasDe(_nombre));
	}

	@Test(expected = IllegalArgumentException.class)
	public void manzanasDeInexistente() {
		_radio.manzanasDe("Oscar");

	}

	@Test(expected = IllegalArgumentException.class)
	public void manzanasDeNombreInvalido() {
		_radio.manzanasDe("");
	}

	@Test
	public void calcularCentro() {
		_radio.agregarCoordenada(0, new Coordenada(4, 8));
		_radio.agregarCoordenada(1, new Coordenada(10, 6));
		_radio.agregarCoordenada(2, new Coordenada(8, 7));
		_radio.agregarCoordenada(3, new Coordenada(6, 10));
		_radio.agregarCoordenada(4, new Coordenada(7, 4));
		Coordinate esperado = new Coordinate(7, 7);

		assertEquals(esperado, _radio.calcularCentro());
	}

	@Test
	public void calcularCentroSinCoordenadas() {
		_radio.calcularCentro();
		Coordinate esperado = new Coordinate(0, 0);

		assertEquals(esperado, _radio.calcularCentro());
	}

	@Test
	public void tieneCensista() {
		_radio.asignarCensista(_nombre, 0);

		assertTrue(_radio.tieneCensista(0));
	}

	@Test
	public void tieneCensistaFalse() {
		assertFalse(_radio.tieneCensista(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void tieneCensistaInvalida() {
		_radio.tieneCensista(-2);
	}

	public RadioCensal crearRadio() {
		RadioCensal radio = new RadioCensal(8);
		radio.unirManzanasContiguas(0, 1);
		radio.unirManzanasContiguas(0, 4);
		radio.unirManzanasContiguas(1, 2);
		radio.unirManzanasContiguas(1, 5);
		radio.unirManzanasContiguas(2, 3);
		radio.unirManzanasContiguas(2, 6);
		radio.unirManzanasContiguas(3, 7);
		radio.unirManzanasContiguas(4, 5);
		radio.unirManzanasContiguas(5, 6);
		radio.unirManzanasContiguas(6, 7);
		return radio;
	}

	private Map<String, List<Integer>> esperadoManzanasPorCensista() {
		Map<String, List<Integer>> esperado = new HashMap<>();
		List<Integer> manzanas = new ArrayList<>();
		manzanas.add(0);
		manzanas.add(2);
		manzanas.add(6);
		esperado.put(_nombre, manzanas);
		List<Integer> manzanas2 = new ArrayList<>();
		manzanas2.add(1);
		manzanas2.add(3);
		manzanas2.add(4);
		esperado.put("Pedro", manzanas2);
		esperado.put("Mario", new ArrayList<Integer>());
		esperado.put("Carlos", new ArrayList<Integer>());
		return esperado;
	}

	private Map<String, List<Integer>> esperadoManzanasPorCensistaLleno() {
		Map<String, List<Integer>> esperado = esperadoManzanasPorCensista();
		List<Integer> manzanas1 = new ArrayList<>();
		manzanas1.add(7);
		List<Integer> manzanas2 = new ArrayList<>();
		manzanas2.add(5);
		esperado.put("Mario", manzanas1);
		esperado.put("Carlos", manzanas2);
		return esperado;
	}
}
