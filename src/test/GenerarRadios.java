package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import negocio.RadioCensal;

public class GenerarRadios {

	public static RadioCensal generarRadio1() {
		RadioCensal radio = new RadioCensal(24);
		int cantCensistas = 9;
		for (int i = 0; i < cantCensistas; i++) {
			radio.agregarCensista("cen" + i);
		}
		radio.unirManzanasContiguas(0, 1);
		radio.unirManzanasContiguas(1, 2);
		radio.unirManzanasContiguas(2, 3);
		radio.unirManzanasContiguas(4, 5);
		radio.unirManzanasContiguas(5, 6);
		radio.unirManzanasContiguas(6, 7);
		radio.unirManzanasContiguas(8, 9);
		radio.unirManzanasContiguas(10, 11);
		radio.unirManzanasContiguas(12, 13);
		radio.unirManzanasContiguas(13, 14);
		radio.unirManzanasContiguas(14, 15);
		radio.unirManzanasContiguas(16, 17);
		radio.unirManzanasContiguas(17, 18);
		radio.unirManzanasContiguas(18, 19);
		radio.unirManzanasContiguas(20, 21);
		radio.unirManzanasContiguas(21, 22);
		radio.unirManzanasContiguas(22, 23);
		radio.unirManzanasContiguas(0, 4);
		radio.unirManzanasContiguas(4, 8);
		radio.unirManzanasContiguas(8, 12);
		radio.unirManzanasContiguas(12, 16);
		radio.unirManzanasContiguas(16, 20);
		radio.unirManzanasContiguas(1, 5);
		radio.unirManzanasContiguas(5, 9);
		radio.unirManzanasContiguas(9, 13);
		radio.unirManzanasContiguas(13, 17);
		radio.unirManzanasContiguas(17, 21);
		radio.unirManzanasContiguas(2, 6);
		radio.unirManzanasContiguas(6, 10);
		radio.unirManzanasContiguas(10, 14);
		radio.unirManzanasContiguas(14, 18);
		radio.unirManzanasContiguas(18, 22);
		radio.unirManzanasContiguas(3, 7);
		radio.unirManzanasContiguas(7, 11);
		radio.unirManzanasContiguas(11, 15);
		radio.unirManzanasContiguas(15, 19);
		radio.unirManzanasContiguas(19, 23);
		return radio;
	}

	public static RadioCensal generarRadio2() {
		RadioCensal radio = new RadioCensal(12);
		int cantCensistas = 5;
		for (int i = 0; i < cantCensistas; i++) {
			radio.agregarCensista("cen" + i);
		}
		radio.unirManzanasContiguas(0, 1);
		radio.unirManzanasContiguas(1, 2);
		radio.unirManzanasContiguas(2, 3);
		radio.unirManzanasContiguas(4, 5);
		radio.unirManzanasContiguas(5, 6);
		radio.unirManzanasContiguas(6, 7);
		radio.unirManzanasContiguas(8, 9);
		radio.unirManzanasContiguas(9, 10);
		radio.unirManzanasContiguas(10, 11);
		radio.unirManzanasContiguas(0, 4);
		radio.unirManzanasContiguas(4, 8);
		radio.unirManzanasContiguas(1, 5);
		radio.unirManzanasContiguas(5, 9);
		radio.unirManzanasContiguas(2, 6);
		radio.unirManzanasContiguas(6, 10);
		radio.unirManzanasContiguas(3, 7);
		radio.unirManzanasContiguas(7, 11);
		return radio;
	}

	public static Map<String, List<Integer>> esperado1() {
		Map<String, List<Integer>> esperado = new HashMap<>();
		List<Integer> manzanas1 = new ArrayList<>();
		manzanas1.add(0);
		manzanas1.add(1);
		manzanas1.add(2);
		esperado.put("cen0", manzanas1);
		List<Integer> manzanas2 = new ArrayList<>();
		manzanas2.add(3);
		manzanas2.add(6);
		manzanas2.add(7);
		esperado.put("cen1", manzanas2);
		List<Integer> manzanas3 = new ArrayList<>();
		manzanas3.add(4);
		manzanas3.add(5);
		manzanas3.add(9);
		esperado.put("cen2", manzanas3);
		List<Integer> manzanas4 = new ArrayList<>();
		manzanas4.add(8);
		manzanas4.add(12);
		manzanas4.add(13);
		esperado.put("cen3", manzanas4);
		List<Integer> manzanas5 = new ArrayList<>();
		manzanas5.add(10);
		manzanas5.add(11);
		manzanas5.add(15);
		esperado.put("cen4", manzanas5);
		List<Integer> manzanas6 = new ArrayList<>();
		manzanas6.add(14);
		manzanas6.add(17);
		manzanas6.add(18);
		esperado.put("cen5", manzanas6);
		List<Integer> manzanas7 = new ArrayList<>();
		manzanas7.add(16);
		manzanas7.add(20);
		manzanas7.add(21);
		esperado.put("cen6", manzanas7);
		List<Integer> manzanas8 = new ArrayList<>();
		manzanas8.add(19);
		manzanas8.add(22);
		manzanas8.add(23);
		esperado.put("cen7", manzanas8);
		esperado.put("cen8", new ArrayList<Integer>());
		return esperado;
	}

	public static Map<String, List<Integer>> esperado2() {
		Map<String, List<Integer>> esperado = new HashMap<>();
		List<Integer> manzanas1 = new ArrayList<>();
		manzanas1.add(0);
		manzanas1.add(1);
		manzanas1.add(2);
		esperado.put("cen0", manzanas1);
		List<Integer> manzanas2 = new ArrayList<>();
		manzanas2.add(3);
		manzanas2.add(6);
		manzanas2.add(7);
		esperado.put("cen1", manzanas2);
		List<Integer> manzanas3 = new ArrayList<>();
		manzanas3.add(4);
		manzanas3.add(5);
		manzanas3.add(9);
		esperado.put("cen2", manzanas3);
		List<Integer> manzanas4 = new ArrayList<>();
		manzanas4.add(8);
		esperado.put("cen3", manzanas4);
		List<Integer> manzanas5 = new ArrayList<>();
		manzanas5.add(10);
		manzanas5.add(11);
		esperado.put("cen4", manzanas5);
		return esperado;
	}

	public static RadioCensal generarRadioPocosCensistas() {
		RadioCensal radio = new RadioCensal(4);
		radio.agregarCensista("cen1");
		radio.unirManzanasContiguas(0, 1);
		radio.unirManzanasContiguas(0, 4);
		radio.unirManzanasContiguas(4, 5);
		radio.unirManzanasContiguas(5, 1);
		return radio;
	}

	public static RadioCensal generarRadioChico() {
		RadioCensal radio = new RadioCensal(8);
		for (int i = 0; i < 3; i++) {
			radio.agregarCensista("cen" + i);
		}
		radio.unirManzanasContiguas(0, 1);
		radio.unirManzanasContiguas(1, 2);
		radio.unirManzanasContiguas(2, 3);
		radio.unirManzanasContiguas(4, 5);
		radio.unirManzanasContiguas(5, 6);
		radio.unirManzanasContiguas(6, 7);
		radio.unirManzanasContiguas(0, 4);
		radio.unirManzanasContiguas(1, 5);
		radio.unirManzanasContiguas(2, 6);
		radio.unirManzanasContiguas(3, 7);
		return radio;
	}
}
