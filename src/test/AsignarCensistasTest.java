package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import negocio.AsignarCensistas;
import negocio.RadioCensal;

public class AsignarCensistasTest {

	private RadioCensal _radio;
	private AsignarCensistas _solver;

	@Before
	public void inicializar() {
		_radio = GenerarRadios.generarRadio1();
		_solver = new AsignarCensistas(_radio);
	}

	@Test
	public void resolverTest() {
		_solver.resolver();
		Map<String, List<Integer>> resultado = _radio.manzanasPorCensista();
		Map<String, List<Integer>> esperado = GenerarRadios.esperado1();

		assertEquals(esperado, resultado);
	}

	@Test
	public void resolverSegundoTest() {
		_radio = GenerarRadios.generarRadio2();
		_solver = new AsignarCensistas(_radio);
		_solver.resolver();
		Map<String, List<Integer>> resultado = _radio.manzanasPorCensista();
		Map<String, List<Integer>> esperado = GenerarRadios.esperado2();

		assertEquals(esperado, resultado);
	}

	@Test(expected = IllegalArgumentException.class)
	public void noAlcanzanLosCensistas() {
		RadioCensal radio = GenerarRadios.generarRadioPocosCensistas();
		AsignarCensistas solver = new AsignarCensistas(radio);
		solver.resolver();
	}

	@Test
	public void asignarUnCensistaTresManzanas() {
		_solver.asignarUnCensista("cen1", 0);
		List<Integer> resultado = _radio.manzanasDe("cen1");
		List<Integer> esperado = new ArrayList<>();
		esperado.add(0);
		esperado.add(1);
		esperado.add(2);

		assertEquals(esperado, resultado);
	}

	@Test
	public void asignarUnCensistaTestDosManzanas() {
		RadioCensal radio = GenerarRadios.generarRadioChico();
		AsignarCensistas solver = new AsignarCensistas(radio);
		solver.resolver();
		List<Integer> resultado = radio.manzanasDe("cen2");
		List<Integer> esperado = new ArrayList<>();
		esperado.add(4);
		esperado.add(5);

		assertEquals(esperado, resultado);
	}

	@Test
	public void asignarUnCensistaUnCensista() {
		RadioCensal radio = GenerarRadios.generarRadioChico();
		AsignarCensistas solver = new AsignarCensistas(radio);
		solver.asignarUnCensista("cen3", 5);
		solver.resolver();
		List<Integer> resultado = radio.manzanasDe("cen1");
		List<Integer> esperado = new ArrayList<>();
		esperado.add(4);

		assertEquals(esperado, resultado);
	}
}
