package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsignarCensistas {

	private List<String> _censistasDisponibles;
	private String[] _manzanas;
	private RadioCensal _radioCensal;

	public AsignarCensistas(RadioCensal radioCensal) {
		_radioCensal = radioCensal;
		_manzanas = radioCensal.getCensistasManzanas();
		_censistasDisponibles = new ArrayList<>();
		_censistasDisponibles.addAll(radioCensal.getCensistas());
	}

	public void resolver() {
		for (int i = 0; i < getManzanas().length; i++) {

			if (getRadioCensal().tieneCensista(i))
				continue;
			if (getCensistasDisponibles().isEmpty())
				noAlcanzanLosCencistas();

			String censista = getCensistasDisponibles().get(0);
			asignarUnCensista(censista, i);
			getCensistasDisponibles().remove(censista);
		}
	}

	public void asignarUnCensista(String censista, int manzana) {
		List<Integer> vecinos = getRadioCensal().vecinosSinCensista(manzana);
		if (vecinos.size() > 0) {
			int vecino1 = vecinos.get(0);
			List<Integer> vecinosDelVecino1 = getRadioCensal().vecinosSinCensista(vecino1);
			Collections.sort(vecinosDelVecino1);
			int vecino2 = vecinosDelVecino1.get(0);
			if (vecino2 != manzana) {
				asignar(censista, manzana);
				asignar(censista, vecino1);
				asignar(censista, vecino2);
			} else {
				if (vecinosDelVecino1.size() > 1) {
					vecino2 = vecinosDelVecino1.get(1);
					asignar(censista, manzana);
					asignar(censista, vecino1);
					asignar(censista, vecino2);
				} else {
					asignar(censista, manzana);
					asignar(censista, vecino1);
				}
			}
		} else {
			asignar(censista, manzana);
		}
	}

	private void noAlcanzanLosCencistas() throws IllegalArgumentException {
		throw new IllegalArgumentException("Los censistas ingresados no alcanzan para todas las manzanas.");
	}

	private void asignar(String censista, int manzana) {
		getRadioCensal().asignarCensista(censista, manzana);
	}

	private List<String> getCensistasDisponibles() {
		return this._censistasDisponibles;
	}

	private String[] getManzanas() {
		return this._manzanas;
	}

	private RadioCensal getRadioCensal() {
		return this._radioCensal;
	}
}