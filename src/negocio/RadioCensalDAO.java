package negocio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class RadioCensalDAO {

	public static RadioCensal leerJSON(String archivo) {

		Gson gson = new Gson();
		RadioCensal radioCensal = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			radioCensal = gson.fromJson(br, RadioCensal.class);
			br.close();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		String[] censistasManzanas = new String[radioCensal.tamano()];
		radioCensal.setCensistasManzanas(censistasManzanas);
		return radioCensal;
	}
}