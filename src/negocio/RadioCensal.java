package negocio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class RadioCensal extends Grafo {

	private List<String> _censistas;
	private String[] _censistasManzanas;
	private List<Coordenada> _coordenadas;

	public RadioCensal() {
		super();
	}

	public RadioCensal(int manzanas) {
		super(manzanas);
		this._censistas = new ArrayList<String>();
		this._censistasManzanas = new String[manzanas];
		this._coordenadas = new ArrayList<>(manzanas);
	}

	public void unirManzanasContiguas(int manzana1, int manzana2) {
		this.agregarArista(manzana1, manzana2);
	}

	public void agregarCensista(String censista) {
		comprobarNombreValido(censista);
		comprobarQueNoExista(censista);
		_censistas.add(censista);
	}

	public void agregarCoordenada(int manzana, Coordenada coor) {
		comprobarManzanaValida(manzana);
		this._coordenadas.add(manzana, coor);
	}

	public void asignarCensista(String nombreCensista, int manzana) {
		comprobarManzanaValida(manzana);
		if (!this._censistas.contains(nombreCensista)) {
			agregarCensista(nombreCensista);
		}
		this._censistasManzanas[manzana] = nombreCensista;
	}

	public void asignarCensistas() {
		AsignarCensistas solver = new AsignarCensistas(this);
		solver.resolver();
	}

	public Map<String, List<Integer>> manzanasPorCensista() {
		Map<String, List<Integer>> censistaManzanas = new HashMap<>();
		for (String censista : getCensistas()) {
			censistaManzanas.put(censista, manzanasDe(censista));
		}
		return censistaManzanas;
	}

	public List<Integer> manzanasDe(String censista) {
		comprobarNombreValido(censista);
		comprobarQueExista(censista);
		List<Integer> manzanas = new ArrayList<>();
		for (int i = 0; i < this.getCensistasManzanas().length; i++) {
			if (getCensistasManzanas()[i] != null && getCensistasManzanas()[i].equals(censista)) {
				manzanas.add(i);
			}
		}
		return manzanas;
	}

	public List<Integer> vecinosSinCensista(int manzana) {
		comprobarManzanaValida(manzana);
		List<Integer> vecinosSinCensista = new ArrayList<>();
		for (int vecino : this.vecinos(manzana)) {
			if (this._censistasManzanas[vecino] == null) {
				vecinosSinCensista.add(vecino);
			}
		}
		return vecinosSinCensista;
	}

	public Coordinate calcularCentro() {
		double x = 0.0;
		double y = 0.0;
		double promedioX = 0.0;
		double promedioY = 0.0;
		int cantManzanas = _coordenadas.size();
		for (Coordenada c : _coordenadas) {
			x += c.get_x();
			y += c.get_y();
		}
		if (x == 0.0 || y == 0.0)
			return new Coordinate(0.0, 0.0);

		promedioX = x / cantManzanas;
		promedioY = y / cantManzanas;

		return new Coordinate(promedioX, promedioY);
	}

	public boolean tieneCensista(int manzana) {
		comprobarManzanaValida(manzana);
		return this._censistasManzanas[manzana] != null;
	}

	public String[] getCensistasManzanas() {
		return _censistasManzanas;
	}

	public List<String> getCensistas() {
		return _censistas;
	}

	public String getCensistaDe(int manzana) {
		return this.getCensistasManzanas()[manzana];
	}

	public List<Coordenada> getCoordenadas() {
		return _coordenadas;
	}

	public void setCensistasManzanas(String[] _censistasManzanas) {
		this._censistasManzanas = _censistasManzanas;
	}

	@Override
	public String toString() {
		return "RadioCensal [_censistas=" + _censistas + ", _censistasManzanas=" + Arrays.toString(_censistasManzanas)
				+ ", _coordenadas=" + _coordenadas + "]";
	}

	private void comprobarManzanaValida(int manzana) throws IllegalArgumentException {
		if (manzana < 0 || manzana >= this.tamano())
			throw new IllegalArgumentException("Numero de manzana no valido: " + manzana);
	}

	private void comprobarQueNoExista(String censista) throws IllegalArgumentException {
		if (this._censistas.contains(censista))
			throw new IllegalArgumentException("El nombre del censista no puede ser vacio");
	}

	private void comprobarNombreValido(String censista) throws IllegalArgumentException {
		if (censista.isEmpty())
			throw new IllegalArgumentException("El nombre del censista no puede ser vacio");
	}

	private void comprobarQueExista(String censista) throws IllegalArgumentException {
		if (!getCensistas().contains(censista)) {
			throw new IllegalArgumentException("Censista no existente");
		}
	}
}